<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="no"/>
<xsl:decimal-format name="no" decimal-separator="," grouping-separator=" "/>
<xsl:decimal-format name="pros" decimal-separator="."/>

<xsl:param name="navn"></xsl:param>
<xsl:param name="tabelltype">0</xsl:param>
<xsl:param name="status">0</xsl:param>

<xsl:variable name="date">
	<xsl:value-of select="/respons/rapport/data[@navn='SisteRegDato']"/>
</xsl:variable>

<xsl:variable name="dato">
	<xsl:value-of select="substring($date, 7, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 5, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 1, 4)"/>
</xsl:variable>

<xsl:template match="/">
<!--<html>-->
	<xsl:choose>
	<xsl:when test="$navn=''">
	<xsl:call-template name="rapporthode"/>	

	<xsl:if test="$tabelltype='1' or $tabelltype='0'">  
		<!-- tabeller i rapporten  -->
		<table class="liste" border="1"  cellspacing="0"  cellpadding="3">
			<xsl:call-template name="tabellhode"/>
			<xsl:apply-templates select="respons/rapport/tabell[@navn='F07tabell1']/liste" mode="prosent"/>
			<!--<xsl:call-template name="sum"/>-->
			<tr>
			<td class="sum">Prog. mand.</td>
			<xsl:apply-templates select="respons/rapport/tabell[@navn='F07tabell2']/liste[data[@navn='PartikategoriL'] &lt; 2]/data[@navn='ProgAntMndFtvL']" mode="sum"/>
			</tr>
		</table>
	</xsl:if>
	<p/>
	<xsl:if test="$tabelltype='2' or $tabelltype='0'">
		<!-- tabeller i mandater pr fylke  -->
		<table class="liste" border="1"  cellspacing="0"  cellpadding="3">
			<tr>
				<th colspan="10">Prognose pr. fylke - Tenkt Stortingsvalg</th>
			</tr>
			<xsl:call-template name="tabellhode"/>
			<xsl:apply-templates select="respons/rapport/tabell[@navn='F07tabell1']/liste" mode="mandat"/>
			<xsl:call-template name="sum-mandater"/>
		</table>
	</xsl:if>
	<br/>

		<!-- tabell for mandater -->
<!--
		<table class="liste" border="1"  cellspacing="0"  cellpadding="3">
			<xsl:call-template name="mandathode"/>
			<xsl:apply-templates select="respons/rapport/tabell[@navn='F07tabell2']/liste[data[@navn='PartikategoriL'] &lt; 2]"/>			
		</table>

		<p/>
-->

<!--	
		<table class="status" border="1"  cellspacing="0"  cellpadding="3">
		<xsl:apply-templates select="respons/rapport/data"/>
		</table>
-->
		<xsl:choose>
		<xsl:when test="$status=1">
			<table class="statusouter" border="1"  cellspacing="0"  cellpadding="0">
				<tr>
				<td valign="top">

          <table class="status" border="0"  cellspacing="0"  cellpadding="5">
            <xsl:apply-templates select="/respons/rapport/data"/>
          </table>

          <!--<table class="status" border="0"  cellspacing="0"  cellpadding="5">
					<xsl:apply-templates select="/respons/rapport/data[(position() div 2) - round(position() div 2) != 0]"/>
				</table>
				</td>
				<td valign="top">
				<table class="status" border="0"  cellspacing="0"  cellpadding="5">
					<xsl:apply-templates select="/respons/rapport/data[(position() div 2) - round(position() div 2) = 0]"/>
				</table>-->
				</td>
				</tr>
			</table>			
		</xsl:when>
		<xsl:otherwise>
			<!--</xsl:otherwise><xsl:apply-templates select="/respons/rapport" mode="status0"/>-->
			<xsl:call-template name="sumkommuner"/>
		</xsl:otherwise>
		</xsl:choose>
	
	</xsl:when>
	<xsl:otherwise>
		<xsl:call-template name="error"/>
	</xsl:otherwise>
	</xsl:choose>
<!--</html>	-->
</xsl:template>

<xsl:template name="error">
	<h2>Resultatet for <xsl:value-of select="$navn"/> er ikke klart ennå!</h2>
</xsl:template>

<!-- Template for sum i tabellen -->
<xsl:template name="sum-mandater">
	<tr>
	<td class="sum">Sum</td>
	<xsl:apply-templates select="respons/rapport/tabell[@navn='F07tabell2']/liste[data[@navn='PartikategoriL'] &lt; 2]/data[@navn='ProgAntDMndtStvL']" mode="sum"/>
	</tr>

	<tr>
	<td class="sum">Utj. rep.</td>
	<xsl:apply-templates select="respons/rapport/tabell[@navn='F07tabell2']/liste[data[@navn='PartikategoriL'] &lt; 2]/data[@navn='ProgAntUMndtStvL']" mode="sum"/>
	</tr>

	<tr>
	<td class="sum">Sum rep.</td>
	<xsl:apply-templates select="respons/rapport/tabell[@navn='F07tabell2']/liste[data[@navn='PartikategoriL'] &lt; 2]/data[@navn='ProgAntMndtStvL']" mode="sum"/>
	</tr>
<!--
	<tr>
	<td class="sum">Sum fylkest.v.</td>
	<xsl:apply-templates select="respons/rapport/tabell[@navn='F07tabell2']/liste[data[@navn='PartikategoriL'] &lt; 2]/data[@navn='ProgAntMndtFtvL']"/mode="sum">
	</tr>
-->	
</xsl:template>

<xsl:template match="data"  mode="sum">
	<td align="Right"><xsl:value-of select="."/></td>
</xsl:template>

<!-- Template for rader i tabellen -->
<xsl:template match="respons/rapport/tabell[@navn='F07tabell1']/liste" mode="prosent">
	<tr>
		<td><xsl:value-of select="data[@navn='FylkeNavn']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='A']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='SV']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='RØDT']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='SP']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='KRF']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='V']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='H']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='FRP']/data[@navn='ProgProSt']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='Andre']/data[@navn='ProgProSt']"/></td>

<!--	 <xsl:apply-templates select="tabell[@navn='F07tabell11']/liste/data[@navn='Partikode' and .='A']"/>-->
	</tr>
</xsl:template>

<!-- Template for rader i tabellen for mandater -->
<xsl:template match="respons/rapport/tabell[@navn='F07tabell1']/liste" mode="mandat">
	<tr>
		<td><xsl:value-of select="data[@navn='FylkeNavn']"/></td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data/@navn='Partikode' and data[@navn='Partikode']='A']/data[@navn='ProgAntMndtStv']"/>

		</td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='SV']/data[@navn='ProgAntMndtStv']"/>

		</td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='RØDT']/data[@navn='ProgAntMndtStv']"/>

		</td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='SP']/data[@navn='ProgAntMndtStv']"/>

		</td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='KRF']/data[@navn='ProgAntMndtStv']"/>

		</td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='V']/data[@navn='ProgAntMndtStv']"/>

		</td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='H']/data[@navn='ProgAntMndtStv']"/>

		</td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='FRP']/data[@navn='ProgAntMndtStv']"/>


		</td>
		<td align="right">&#160;<xsl:value-of select="tabell/liste[data[@navn='Partikode']='Andre']/data[@navn='ProgAntMndtStv']"/>

		</td>
<!--	 <xsl:apply-templates select="tabell[@navn='F07tabell11']/liste/data[@navn='Partikode' and .='A']"/>-->
	</tr>
</xsl:template>

<xsl:template match="respons/rapport/tabell[@navn='F07tabell2']/liste">
	<tr><xsl:apply-templates select="data[@navn!='PartikategoriL']"/></tr>
</xsl:template>
<xsl:template match="data[1]">
	<td><xsl:value-of select="."/></td>
</xsl:template>
<xsl:template match="data">
	<td align="right"><xsl:value-of select="."/></td>
</xsl:template>

<xsl:template match="tabell[@navn='F07tabell11']/liste">
	<td><xsl:value-of select="data[@navn='Partikode']"/>: <xsl:value-of select="data[@navn='ProgProSt']"/></td>
</xsl:template>


<!-- Template for kolonner i tabell header-->
<xsl:template name="tabellhode">
	<tr>
		<th>Fylke</th>
		<th>A</th>
		<th>SV</th>
		<th>RØDT</th>
		<th>SP</th>
		<th>KRF</th>
		<th>V</th>
		<th>H</th>
		<th>FRP</th>
		<th>Andre</th>
	</tr>
</xsl:template>

<!-- Template for kolonner i tabell header-->
<xsl:template name="mandathode">
	<tr>
		<th colspan="5">Prognose tenkt Stortingsvalg</th>
	</tr>
	<tr>
		<th>Parti</th>
		<th>Fylkestings-<br/>mandater</th>
		<th>Distriktsmandater</th>
		<th>Utjamningsmandater</th>
		<th>Totalt antall mandater</th>
	</tr>
</xsl:template>

<xsl:template name="rapporthode">
	<xsl:variable name="rapportnavn">
		<xsl:value-of select="respons/rapport/rapportnavn"/>
	</xsl:variable>
	
	<h1>
	<xsl:value-of select="$rapportnavn"/>
	<xsl:text>: </xsl:text>

	<xsl:choose>
		<xsl:when test="$rapportnavn = 'F01'">
			<xsl:text>Opptalte kommuner - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F02'">
			<xsl:text>Enkeltresultat pr. kommune - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F03'">
			<xsl:text>Enkeltoversikt pr. krets - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F04'">
			<xsl:text>Fylkesoversikt - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F05'">
			<xsl:text>Landsoversikt pr. parti - fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'F07' and ($tabelltype='1' or $tabelltype='0')">
			<xsl:text>Landsoversikt pr. fylke - fylkestingsvalg</xsl:text>
		</xsl:when>
    <xsl:when test="$rapportnavn = 'F07' and $tabelltype='2'">
      <xsl:text>Landsoversikt pr. fylke - Tenkt stortingsvalg</xsl:text>
    </xsl:when>
    <xsl:when test="$rapportnavn = 'ST07'">
			<xsl:text>Landsoversikt pr. fylke - stortingstingsvalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K01'">
			<xsl:text>Opptalte kommuner - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K02'">
			<xsl:text>Enkeltresultat pr. kommune - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K03'">
			<xsl:text>Enkeltoversikt pr. krets - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K04'">
			<xsl:text>Fylkesoversikt - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K05'">
			<xsl:text>Landsoversikt pr. parti - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K07'">
			<xsl:text>Frammøte – 10 høyeste og 10 laveste - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K08'">
			<xsl:text>Bydelsresultater i Oslo - kommunevalg</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K09'">
			<xsl:text>Bystyreoversikt - kommunevalg</xsl:text>
		</xsl:when>
	</xsl:choose>
	</h1>

	<xsl:choose>
		<xsl:when test="rapport/data[@navn='KommNavn']">
			<h2>
			<xsl:value-of select="rapport/data[@navn='KommNavn']"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="rapport/data[@navn='FylkeNavn']"/>
			</h2>
		</xsl:when>
		<xsl:when test="rapport/data[@navn='FylkeNavn']">
			<h2>
			<xsl:value-of select="rapport/data[@navn='FylkeNavn']"/>
			</h2>
		</xsl:when>
	</xsl:choose>
	
	<p class="regdato"><b>Siste registrering: <xsl:value-of select="$dato"/>&#160;<xsl:value-of select="respons/rapport/data[@navn='SisteRegTid']"/></b></p>

</xsl:template>

<!-- Template for status-rader i rapporten  -->
<xsl:template match="respons/rapport/data">
	<tr>
	<td><xsl:value-of select="@navn"/>:</td>
	<td align="right">
	<xsl:attribute name="class">
		<xsl:value-of select="@navn"/>
	</xsl:attribute>
	<xsl:value-of select="."/>
	</td>
	</tr>
</xsl:template>


<xsl:template match="respons/rapport/tabell[@navn='F07tabell1']" >
	<xsl:value-of select="format-number(sum(liste/data[@navn='TotAntKomm']), '###')"/>
</xsl:template>

<!-- summerer opp alle opptalte kommuner-->
<xsl:template name="sumkommuner">
	<xsl:value-of select="sum(respons/rapport/tabell[@navn='F07tabell1']/liste/data[@navn='AntKommFhstOpptalt'])
	 + sum(respons/rapport/tabell[@navn='F07tabell1']/liste/data[@navn='AntKommVtstOpptalt']) + sum(respons/rapport/tabell[@navn='F07tabell1']/liste/data[@navn='AntKommAltOpptalt'])"/>
	<xsl:text> av </xsl:text>
	<xsl:value-of select="format-number(sum(respons/rapport/tabell[@navn='F07tabell1']/liste/data[@navn='TotAntKomm']), '### ###')"/>	
	<xsl:text> kommuner opptalt.</xsl:text><br/>
	<xsl:text>Omfatter </xsl:text>
	<xsl:value-of select="format-number((sum(respons/rapport/tabell[@navn='F07tabell1']/liste/data[@navn='AntFrammotte']) div sum(respons/rapport/tabell[@navn='F07tabell1']/liste/data[@navn='AntStBerett']) * 100), '0.0', 'pros')"/>	
	<xsl:text> prosent av totalt antall stemmeberettigede.</xsl:text><br/>	

	<xsl:value-of select="format-number(sum(respons/rapport/tabell[@navn='F07tabell1']/liste/data[@navn='AntFrammotte']), '### ### ###', 'no')"/>
	<xsl:text> opptalte stemmer.</xsl:text><br/>

</xsl:template>

  <!-- Template for status-rader i rapporten  -->
  <xsl:template match="/respons/rapport/data">
    <tr>
      <xsl:attribute name="class">
        <xsl:value-of select="@navn"/>
      </xsl:attribute>
      <td class="ledetekst">
        <xsl:call-template name="ledetekst">
          <xsl:with-param name="navn">
            <xsl:value-of select="@navn"/>
          </xsl:with-param>
        </xsl:call-template>
      </td>
      <td class="colon">:</td>
      <td align="right">
        <xsl:attribute name="class">
          <xsl:value-of select="@navn"/>
        </xsl:attribute>
        <xsl:value-of select="."/>
      </td>
    </tr>
  </xsl:template>


  <!-- Template for endring av ledetekster -->
  <xsl:template name="ledetekst">
    <xsl:param name="navn"/>
    <xsl:choose>
      <xsl:when test="$navn='SisteRegDato'">
        <xsl:text>Registert dato</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='SisteRegTid'">
        <xsl:text>Registert tid</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='StatusInd'">
        <xsl:text>Indikator for opptellingsstatus</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='FylkeNr'">
        <xsl:text>Fylkenummer</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='FylkeNavn'">
        <xsl:text>Fylkenavn</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='KretsNr'">
        <xsl:text>Kretsummer</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='KretsNavn'">
        <xsl:text>Kretsnavn</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='KommNr'">
        <xsl:text>Kommunenummer</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='KommNavn'">
        <xsl:text>Kommunenavn</xsl:text>
      </xsl:when>

      <xsl:when test="$navn='DiffFrammFFtv'">
        <xsl:text>Endring i antall frammøtte i forhold til forrige fylkestingsvalg</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='DiffPropFrammFFtv'">
        <xsl:text>Endring i prosentpoeng for antall frammøtte i forhold til forrige fylkestingsvalg</xsl:text>
      </xsl:when>

      <xsl:when test="$navn='DiffFrammFKsv'">
        <xsl:text>Endring i ant. frammøtte i forhold til forrige kommunestyrevalg</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='DiffPropFrammFKsv'">
        <xsl:text>Endring i prosentpoeng for ant. frammøtte i forhold til forrige kommunestyrevalg</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='DiffFrammFStv'">
        <xsl:text>Endring i ant. frammøtte i forhold til forrige stortingsvalg</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='DiffPropFrammFStv'">
        <xsl:text>Endring i prosentpoeng for ant. frammøtte i forhold til forrige stortingsvalg</xsl:text>
      </xsl:when>

      <xsl:when test="$navn='TotAntKomm'">
        <xsl:text>Totalt antall kommuner</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKommFhstOpptalt'">
        <xsl:text>Antall kommuner hvor bare forhåndsstemmer er opptalt.</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKommVtstOpptalt'">
        <xsl:text>Antall kommuner hvor bare valgtingsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKommAltOpptalt'">
        <xsl:text>Antall kommuner hvor både forhånds- og valgtingsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='TotAntKretser'">
        <xsl:text>Totalt antall kretser i fylket</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKretserFhstOpptalt'">
        <xsl:text>Antall kretser hvor bare forhåndsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKretserVtstOpptalt'">
        <xsl:text>Antall kretser hvor bare valgtingsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKretserAltOpptalt'">
        <xsl:text>Antall kretser hvor både forhånds- og valgtingsstemmer er opptalt</xsl:text>
      </xsl:when>

      <xsl:when test="$navn='AntStBerett' or $navn='AntStberett'">
        <xsl:text>Antall stemmeberettigede</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntFrammotte'">
        <xsl:text>Antall frammøtte</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='ProFrammotte'">
        <xsl:text>Frammøteprosent</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntFhstOpptalt'">
        <xsl:text>Antall forhåndsstemmer</xsl:text>
      </xsl:when>

      <!-- Added for Valg2005 -->

      <xsl:when test="$navn='AntRepKvinner'">
        <xsl:text>Antall kvinnelige representanter </xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntRepMenn'">
        <xsl:text>Antall mannlige representanter </xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntRepUkjKjonn'">
        <xsl:text>Antall representanter hvor kjønnet er ukjent</xsl:text>
      </xsl:when>

      <!-- Added for Valg2009 -->
      <xsl:when test="$navn='StDevFylke'">
        <xsl:text>Standardavvik for fylket</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='ProgProStOpptalt'">
        <xsl:text>Prognose for prosentandel av stemmene som er opptalt til nå</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKommAltOppt8'">
        <xsl:text>Antall kommuner hvor alle stemmer er opptalt og kontrollert</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKretserAltOppt8'">
        <xsl:text>Antall kretser hvor alle stemmer er opptalt og kontrollert</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='PartikodeMiste'">
        <xsl:text>Partikode for partiet som er nærmest å miste sistemandatet i fylket</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='PartikodeVinne'">
        <xsl:text>Partikode for partiet som er nærmest å vinne sistemandatet i fylket</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntStMiste'">
        <xsl:text>Antall stemmer partiet kan miste uten å miste sistemandatet</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntStVinne'">
        <xsl:text>Antall stemmer partiet må ha for å vinne sistemandatet</xsl:text>
      </xsl:when>

      <!-- Added for Valg2011 -->
      <xsl:when test="$navn='StDevLand'">
        <xsl:text>Standardavvik for hele landet</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='TotAntKommL'">
        <xsl:text>Totalt antall kommuner</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKommAltOppt8L'">
        <xsl:text>Antall kommuner hvor alle stemmer er opptalt og kontrollert</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKretserAltOppt8L'">
        <xsl:text>Antall kretser hvor alle stemmer er opptalt og kontrollert</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKommFhstOpptaltL'">
        <xsl:text>Antall kommuner hvor bare forhåndsstemmer er opptalt.</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKommVtstOpptaltL'">
        <xsl:text>Antall kommuner hvor bare valgtingsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKommAltOpptaltL'">
        <xsl:text>Antall kommuner hvor både forhånds- og valgtingsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='TotAntKretserL'">
        <xsl:text>Totalt antall kretser</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKretserFhstOpptaltL'">
        <xsl:text>Antall kretser hvor bare forhåndsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKretserVtstOpptaltL'">
        <xsl:text>Antall kretser hvor bare valgtingsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntKretserAltOpptaltL'">
        <xsl:text>Antall kretser hvor både forhånds- og valgtingsstemmer er opptalt</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntStBerettL'">
        <xsl:text>Antall stemmeberettigede</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='AntFrammotteL'">
        <xsl:text>Antall frammøtte</xsl:text>
      </xsl:when>
      <xsl:when test="$navn='ProFrammotteL'">
        <xsl:text>Frammøteprosent</xsl:text>
      </xsl:when>

      <xsl:otherwise>
        <xsl:value-of select="$navn"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

</xsl:stylesheet>