<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes" indent="yes" standalone="yes"/>

<xsl:param name="FylkeNr">12</xsl:param>
<xsl:param name="xmlpath">valg-xml-inn</xsl:param>
<xsl:param name="bydel">no</xsl:param>

  <!--
<xsl:param name="xmlpathfylke">..\<xsl:value-of select="$xmlpath"/>\ST04-</xsl:param>
<xsl:param name="xmlpathfylke03">..\<xsl:value-of select="$xmlpath"/>\ST04-</xsl:param>
<xsl:param name="xmlpathkommune">..\<xsl:value-of select="$xmlpath"/>\ST02-</xsl:param>
<xsl:param name="xmlpathkrets">..\<xsl:value-of select="$xmlpath"/>\ST03.XML</xsl:param>
<xsl:param name="xmlpathbydel">..\<xsl:value-of select="$xmlpath"/>\ST05.XML</xsl:param>
-->

<xsl:param name="xmlpathfylke">..\<xsl:value-of select="$xmlpath"/>\F04-</xsl:param>
<xsl:param name="xmlpathfylke03">..\<xsl:value-of select="$xmlpath"/>\K04-</xsl:param>
<xsl:param name="xmlpathkommune">..\<xsl:value-of select="$xmlpath"/>\K02-</xsl:param>
<xsl:param name="xmlpathkrets">..\<xsl:value-of select="$xmlpath"/>\K03.XML</xsl:param>
<xsl:param name="xmlpathbydel">..\<xsl:value-of select="$xmlpath"/>\K08.XML</xsl:param>

  <xsl:template match="respons">
	<xsl:choose>
		<xsl:when test="$FylkeNr=0">
		<group>
			<xsl:apply-templates select="rapport"/>
		</group>			
		</xsl:when>
		<xsl:otherwise>
		<group>
			<xsl:apply-templates select="rapport[data[@navn='FylkeNr']=$FylkeNr]"/>
		</group>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>


<xsl:template match="rapport">
	<xsl:variable name="path">
		<xsl:choose>
			<xsl:when test="data[@navn='FylkeNr']=03">
				<xsl:value-of select="concat($xmlpathfylke03, data[@navn='FylkeNr'], '.xml')"/>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="concat($xmlpathfylke, data[@navn='FylkeNr'], '.xml')"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>
<!--
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
-->
	<!-- Fylkestabell -->
<!--	
	<tr>
	<td colspan="3">
-->
  <xsl:choose>
    <xsl:when test="data[@navn='FylkeNr']=03">
      <h2>Kommunestyrevalg</h2>
    </xsl:when>
    <xsl:otherwise>
      <h2>Fylkestingsvalg</h2>
    </xsl:otherwise>
  </xsl:choose>

  <table width="100%" border="1" cellpadding="3" cellspacing="0">
		<tr>
			<td rowspan="3" class="fylkegif" width="80" align="center">
				<xsl:attribute name="id">
					<xsl:value-of select="data[@navn='FylkeNr']"/>
				</xsl:attribute>
				<img border="0" width="45" height="55">
					<xsl:attribute name="src">Images\fylke-<xsl:value-of select="data[@navn='FylkeNr']"/>.gif</xsl:attribute>
				</img>
			</td>
			<td class="fylkehead" colspan="9" valign="middle" align="center"><xsl:value-of select="data[@navn='FylkeNavn']"/></td>
		</tr>
	
		<tr>
			<td colspan="5">
			<xsl:text>Fremmøte: </xsl:text>
			<xsl:value-of select="document($path)/respons/rapport/data[@navn='ProFrammotte']"/>
			<xsl:text>%</xsl:text>
			</td>
      <td colspan="4" align="right">
        <xsl:text>Sist oppdatert: </xsl:text>
        <xsl:value-of select="substring(data[@navn='SisteRegDato'], 7, 2)"/>
        <xsl:text>.</xsl:text>
        <xsl:value-of select="substring(data[@navn='SisteRegDato'], 5, 2)"/>
        <xsl:text>.</xsl:text>
        <xsl:value-of select="substring(data[@navn='SisteRegDato'], 1, 4)"/>
        <xsl:text> - </xsl:text>
        <xsl:value-of select="document($path)/respons/rapport/data[@navn='SisteRegTid']"/>
      </td>
    </tr>
	
		<xsl:call-template name="fylke">
			<xsl:with-param name="fylkenr">
				<xsl:value-of select="data[@navn='FylkeNr']"/>
			</xsl:with-param>
			<xsl:with-param name="path">
				<xsl:value-of select="$path"/>
			</xsl:with-param>
		</xsl:call-template>

	</table>
<!--
	</td>
	</tr>
-->
  <xsl:choose>
    <xsl:when test="data[@navn='FylkeNr']=03">
      <h2>Krets/bydelsoversikt - Oslo kommune</h2>
      <!--<table width="100%">
        --><!-- Forklaringstekst --><!--
        <tr class="valgtype">
          <td width="280" valign="bottom" align="left">
            <img border="0" src="images/down.gif"/>Krets/bydelsoversikt, Oslo kommune
          </td>
          <td width="280" valign="top" align="left">
            <img border="0" src="images/up.gif"/>Kommunestyrevalg, Oslo kommune
          </td>
          <td align="right">
          </td>
        </tr>
      </table>-->
    </xsl:when>
    <xsl:otherwise>
      <h2>Kommunestyrevalg</h2>
      <!--<table width="100%">
        --><!-- Forklaringstekst --><!--
        <tr class="valgtype">
          <td width="150" valign="bottom" align="left">
            <img border="0" src="images/down.gif"/>Kommunestyrevalg
          </td>
          <td width="150" valign="top" align="left">
            <img border="0" src="images/up.gif"/>Fylkestingsvalg
          </td>
          --><!--<td valign="bottom" align="center">(Kl. <xsl:value-of select="substring(data[@navn='SisteRegTid'], 1, 5)"/>)</td>--><!--
          <td align="right">
            --><!--<xsl:text>Sist oppdatert: </xsl:text>
      <xsl:value-of select="document($path)/respons/rapport/data[@navn='SisteRegDato']"/>
      <xsl:value-of select="document($path)/respons/rapport/data[@navn='SisteRegTid']"/>--><!--
          </td>
        </tr>
      </table>-->
    </xsl:otherwise>
</xsl:choose>

  <!-- Kommunetabell -->
<!--
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
	<td colspan="3">
-->	
		<table width="100%" border="1" cellpadding="3" cellspacing="0">
		
		<xsl:choose>
		<xsl:when test="data[@navn='FylkeNr']=03">
			<xsl:choose>
				<xsl:when test="$bydel!='yes'">
					<xsl:call-template name="kretshead"/>
					<xsl:for-each select="document($xmlpathkrets)/respons/rapport[data[@navn='KommNr']='0301']">
						<tr>
							<td><xsl:value-of select="data[@navn='KretsNr']"/></td>
							<td><xsl:value-of select="data[@navn='KretsNavn']"/></td>
		
							<xsl:for-each select="tabell/liste[data[@navn='Partikategori']&lt;2 and data[@navn='Partikode'] != 'Andre2']">
								<!--<td><xsl:value-of select="data[@navn='Partikode']"/>:-->
								<td align="right"><xsl:value-of select="data[@navn='ProSt']"/></td>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="bydelhead"/>
					<!--<xsl:for-each select="document($xmlpathbydel)/respons/rapport[data[@navn='BydelNr']!='0' and data[@navn='BydelNr']!='00']">-->
					<xsl:for-each select="document($xmlpathbydel)/respons/rapport">
						<tr>
							<td><xsl:value-of select="data[@navn='BydelNr']"/></td>
							<td>
                <!--xsl:call-template name="bydel">
								<xsl:with-param name="bydelnr"><xsl:value-of select="data[@navn='BydelNr']"/></xsl:with-param>
							</xsl:call-template-->
							<xsl:value-of select="data[@navn='BydelNavn']"/>
							</td>
		
							<xsl:for-each select="tabell/liste[data[@navn='Partikategori']&lt;2 and data[@navn='Partikode'] != 'Andre2']">
								<!--<td><xsl:value-of select="data[@navn='Partikode']"/>:-->
								<td align="right"><xsl:value-of select="data[@navn='ProSt']"/></td>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</xsl:otherwise>						
			</xsl:choose>
		</xsl:when>
		<xsl:otherwise>
			<xsl:call-template name="kommunehead"/>
			<xsl:call-template name="kommunekrets"/>
			<xsl:apply-templates select="tabell/liste">
				<xsl:sort select="data[@navn='KommNavn']"/>
			</xsl:apply-templates>
		</xsl:otherwise>	
		</xsl:choose>
			
		</table>

<!--
	</td>
	</tr>
	</table>
-->	
	<br/>
</xsl:template>

<xsl:template name="bydel">
	<xsl:param name="bydelnr"></xsl:param>
	<xsl:choose>
		<xsl:when test="$bydelnr='01'">
			<xsl:text>Frogner</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='02'">
			<xsl:text>St. Hanshaugen</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='03'">
			<xsl:text>Sagene</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='04'">
			<xsl:text>Grünerløkka</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='05'">
			<xsl:text>Gamle Oslo</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='06'">
			<xsl:text>Nordstrand</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='07'">
			<xsl:text>Søndre Nordstrand</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='08'">
			<xsl:text>Østensjø</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='09'">
			<xsl:text>Alna</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='10'">
			<xsl:text>Stovner</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='11'">
			<xsl:text>Grorud</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='12'">
			<xsl:text>Bjerke</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='13'">
			<xsl:text>Nordre Aker</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='14'">
			<xsl:text>Vestre Aker</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='15'">
			<xsl:text>Ullern</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='16'">
			<xsl:text>Sentrum</xsl:text>
		</xsl:when>
		<xsl:when test="$bydelnr='17'">
			<xsl:text>Marka</xsl:text>
		</xsl:when>
		
		<xsl:otherwise>
			<xsl:text>Stemmer, ikke fordelt</xsl:text>
		</xsl:otherwise>	
	</xsl:choose>

</xsl:template>

<xsl:template name="fylke">
	<xsl:param name="fylkenr"></xsl:param>
	<xsl:param name="path"></xsl:param>

	<!--<xsl:value-of select="concat($xmlpathfylke, $fylkenr)"/>-->
	<tr>
	<xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2 and data[@navn='Partikode'] != 'Andre2']">
		<th><xsl:value-of select="data[@navn='Partikode']"/></th>
	</xsl:for-each>
	</tr>

	<tr>
	<td>Valg 2011</td>
	<xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2 and data[@navn='Partikode'] != 'Andre2']">
		<td align="right"><xsl:value-of select="data[@navn='ProSt']"/></td>
	</xsl:for-each>
	</tr>

	<tr>
	<td>2011 - 2009</td>
	<xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2 and data[@navn='Partikode'] != 'Andre2']">
		<td align="right"><xsl:value-of select="data[@navn='DiffPropFStv']"/></td>
	</xsl:for-each>
	</tr>

	<td>2011 - 2007</td>
	<xsl:for-each select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikategori']&lt;2 and data[@navn='Partikode'] != 'Andre2']">
		<td align="right">
		<xsl:text></xsl:text>
		<xsl:choose>
		<xsl:when test="$fylkenr=03">
			<xsl:value-of select="data[@navn='DiffPropFKsv']"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="data[@navn='DiffPropFFtv']"/>
		</xsl:otherwise>
		</xsl:choose>
		</td>
	</xsl:for-each>

</xsl:template>

<xsl:template match="tabell/liste">
	<!-- Måtte endres for Valg2005 for å fjerne alle kretser i kommunelisten
		 Gjelder kun for Bergen, Stavanger og Trondheim-->
	<xsl:if test="data[@navn='KommNavn'] != 'Bergen' and data[@navn='KommNavn'] != 'Stavanger' and data[@navn='KommNavn'] != 'Trondheim'">
		<tr>
		<td>
		<xsl:value-of select="data[@navn='KommNavn']"/>
		</td>
		<xsl:call-template name="kommune">
			<xsl:with-param name="kommunenr">
			<xsl:value-of select="data[@navn='KommNr']"/>
			</xsl:with-param>
		</xsl:call-template>
		</tr>
	</xsl:if>
</xsl:template>

<!-- Denne funkskjonen er skreddersydd for å
     skrive ut Bergen, Stavanger og Trondheim kommune
     uten å vise kretsen. Disse kommunene er hardkodet-->
<xsl:template name="kommunekrets">
	<xsl:choose>
		<xsl:when test="$FylkeNr=12">
			<tr><td>Bergen</td>
			<xsl:call-template name="kommune">
				<xsl:with-param name="kommunenr">
				<xsl:text>1201</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
			</tr>
		</xsl:when>
		<xsl:when test="$FylkeNr=11">
			<tr><td>Stavanger</td>
			<xsl:call-template name="kommune">
				<xsl:with-param name="kommunenr">
				<xsl:text>1103</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
			</tr>
		</xsl:when>
		<xsl:when test="$FylkeNr=16">
			<tr><td>Trondheim</td>
			<xsl:call-template name="kommune">
				<xsl:with-param name="kommunenr">
				<xsl:text>1601</xsl:text>
				</xsl:with-param>
			</xsl:call-template>
			</tr>
		</xsl:when>
	</xsl:choose>
</xsl:template>


<xsl:template name="kommune">
	<xsl:param name="kommunenr"></xsl:param>
	
	<xsl:variable name="path">
		<xsl:value-of select="concat($xmlpathkommune, $kommunenr, '.xml')"/>
	</xsl:variable>
	
	<!--
	<td><xsl:value-of select="concat($xmlpathkommune, $kommunenr, '.xml')"/></td>	
	-->
	<td align="right">
    <xsl:text>&#160;</xsl:text>
    <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='A']/data[@navn='ProSt']"/>
  </td>
	<td align="right">
    <xsl:text>&#160;</xsl:text>
    <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='SV']/data[@navn='ProSt']"/>
  </td>
	<td align="right">
    <xsl:text>&#160;</xsl:text>
    <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='RØDT']/data[@navn='ProSt']"/>
  </td>
	<td align="right">
    <xsl:text>&#160;</xsl:text>
    <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='SP']/data[@navn='ProSt']"/>
  </td>
  <td align="right">
    <xsl:text>&#160;</xsl:text>
    <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='KRF']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
    <xsl:text>&#160;</xsl:text>
    <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='V']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
    <xsl:text>&#160;</xsl:text>
    <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='H']/data[@navn='ProSt']"/>
  </td>
  <td align="right">
    <xsl:text>&#160;</xsl:text>
    <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='FRP']/data[@navn='ProSt']"/>
	</td>
	<td align="right">
    <xsl:text>&#160;</xsl:text>
    <xsl:value-of select="document($path)/respons/rapport/tabell/liste[data[@navn='Partikode']='Andre']/data[@navn='ProSt']"/>
	</td>
</xsl:template>

<xsl:template name="kommunehead">
	<tr>
	<th>Kommune</th>
	<th>A</th>
	<th>SV</th>
	<th>RØDT</th>
	<th>SP</th>
	<th>KRF</th>
	<th>V</th>
	<th>H</th>
	<th>FRP</th>
	<th>Andre</th>
	</tr>
</xsl:template>

<xsl:template name="kretshead">
	<tr>
	<th>Kretsnr.</th>
	<th>Krets</th>
	<th>A</th>
	<th>SV</th>
	<th>RØDT</th>
	<th>SP</th>
	<th>KRF</th>
	<th>V</th>
	<th>H</th>
	<th>FRP</th>
	<th>Andre</th>
	</tr>
</xsl:template>

<xsl:template name="bydelhead">
	<tr>
	<th>Bydelnr.</th>
	<th>Bydel</th>
	<th>A</th>
	<th>SV</th>
	<th>RØDT</th>
	<th>SP</th>
	<th>KRF</th>
	<th>V</th>
	<th>H</th>
	<th>FRP</th>
	<th>Andre</th>
	</tr>
</xsl:template>

</xsl:stylesheet>