<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="yes"/>

<xsl:template match="/">
	<TABLE BORDER="1" BGCOLOR="#ffffff" CELLSPACING="0" CELLPADDING="3">
		<xsl:call-template name="tablehead"/>
	  <xsl:apply-templates select="dataroot/Query_MAIN"/>
		
	</TABLE>
</xsl:template>

<xsl:template match="dataroot/Query_MAIN">
	<tr>
	<td><xsl:value-of select="partikode"/></td>
	<td><xsl:value-of select="partinavn"/></td>
	<td><xsl:value-of select="kommune"/>&#160;
</td>
    <td align="center">
      <xsl:value-of select="kommnr"/>&#160;
    </td>
    <td>
      <xsl:value-of select="type"/>
    </td>
	</tr>
</xsl:template>

<xsl:template name="tablehead">
	<TR>
	<TH style="width: 2cm">
	<xsl:text>Partikode</xsl:text>
	</TH>
	<TH style="width: 8cm">
	<xsl:text>Partinavn</xsl:text>
	</TH>
	<TH style="width: 5cm">
	<xsl:text>Stiller i</xsl:text>
	</TH>
	<TH style="width: 2cm">
	<xsl:text>Kommunenr.</xsl:text>
	</TH>
	<TH style="width: 3cm">
	<xsl:text>Partitype</xsl:text>
	</TH>
	</TR>
</xsl:template>

<!--<xsl:template match="qPartiliste">

</xsl:template>-->

</xsl:stylesheet>