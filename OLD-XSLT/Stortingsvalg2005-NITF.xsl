<?xml version="1.0"?>
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!--	xmlns:msxsl="urn:schemas-microsoft-com:xslt" -->

<!-- Valget NITF format -->

<xsl:output encoding="ISO-8859-1" method="xml" omit-xml-declaration="no" indent="yes" standalone="yes"/>
<xsl:decimal-format name="no" decimal-separator="," grouping-separator=" "/>
<xsl:decimal-format name="european" decimal-separator="," grouping-separator="."/>
<xsl:decimal-format name="pros" decimal-separator="," grouping-separator=" "/>
<xsl:strip-space elements="*"/>

<xsl:param name="distribusjon">ALL</xsl:param>
<xsl:variable name="fylke">
	<xsl:value-of select="/respons/rapport/data[@navn='FylkeNavn']"/>
</xsl:variable>

<xsl:variable name="fylkenr">
	<xsl:value-of select="/respons/rapport/data[@navn='FylkeNr']"/>
</xsl:variable>

<xsl:variable name="kommune">
	<xsl:value-of select="/respons/rapport/data[@navn='KommNavn']"/>
</xsl:variable>

<xsl:variable name="kommunenr">
	<xsl:value-of select="/respons/rapport/data[@navn='KommNr']"/>
</xsl:variable>

<xsl:variable name="krets">
	<xsl:value-of select="/respons/rapport/data[@navn='KretsNavn']"/>
</xsl:variable>

<xsl:variable name="kretsnr">
	<xsl:value-of select="/respons/rapport/data[@navn='KretsNr']"/>
</xsl:variable>

<!-- /respons/rapport/tabell/liste[data[@navn='Partikode'] = 'A']/data[@navn='ProgProSt'] != '0' and -->

<xsl:variable name="prognose">
	<xsl:value-of select=
		"/respons/rapport/tabell/liste[data[@navn='Partikode'] = 'A']/data[@navn='ProgProSt'] != '' and
		(/respons/rapport/data[@navn='TotAntKomm'] - /respons/rapport/data[@navn='AntKommVtstOpptalt'] - /respons/rapport/data[@navn='AntKommAltOpptalt']) != 0"
	/>
</xsl:variable>

<!-- Main template: -->
<xsl:template match="respons">
<!--
	<xsl:text>@HEADER:APO;INN;;;;</xsl:text>
-->
	<nitf version="-//IPTC//DTD NITF 3.2//EN" change.date="October 10, 2003" change.time="19:30" baselang="no-NO">
	<xsl:call-template name="NitfHead"/>
	<body>
	<body.head>
	<hedline>
	<hl1>
	<!-- Infolinje -->
	<xsl:value-of select="$infolinje"/>
	</hl1>	
	</hedline>
	<distributor><org>NTB</org></distributor>
	</body.head>
	
	<body.content>
		<!--
		<xsl:call-template name="body"/>
		<table border="1" cellspacing="0" cellpadding="5">
			<xsl:call-template name="TableHeader"/>
			<xsl:apply-templates select="rapport"/>
			<xsl:call-template name="sum"/>
		</table>
		<xsl:apply-templates select="rapport" mode="status"/>
		-->
	
	<!-- Topptekster -->
	<xsl:choose>
		<xsl:when test="$rapport='4'">
			<p lede="true" class="lead">
			<xsl:choose>
				<xsl:when test="$prognose = 'true'">
					<xsl:text>Fylkesoversikt, prognose, kl. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Fylkesoversikt, kl. </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="$tid"/>
			</p>
			<p>
			<xsl:value-of select="$fylke"/>
			</p>
		</xsl:when>
		<xsl:when test="$rapport='6'">
			<p lede="true" class="lead">
			<xsl:choose>
				<xsl:when test="$prognose = 'true'">
					<xsl:text>Landsoversikt, prognose, kl. </xsl:text>
				</xsl:when>
				<xsl:otherwise>
					<xsl:text>Landsoversikt, kl. </xsl:text>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:value-of select="$tid"/>
			</p>
		</xsl:when>
		<xsl:when test="$rapport='2'">
			<p lede="true" class="lead">
			<xsl:value-of select="$fylke"/>
			</p>
			<p>
			<xsl:value-of select="$kommune"/>
			</p>
		</xsl:when>
		<xsl:when test="$rapport='3'">
			<p lede="true" class="lead">
			<xsl:value-of select="$kommune"/>
			</p>
			<p>
			<xsl:value-of select="$krets"/>
			</p>
		</xsl:when>
		<xsl:otherwise>
		</xsl:otherwise>
	</xsl:choose>

	<xsl:if test="$rapportnavn ='ST04' or $rapportnavn ='ST06'">
		<xsl:apply-templates select="rapport" mode="status"/>
	</xsl:if>

	<!-- Valg Tabellen -->
	<table>
	<xsl:call-template name="table_header"/>
	<xsl:apply-templates select="rapport"/>
 	<!--xsl:if test="$tabelltype='ov' and $rapportnavn!='ST02' and $rapportnavn!='ST04'">
		<xsl:call-template name="sumandre"/>
	</xsl:if-->		
	<xsl:call-template name="sum"/>
	</table>

 
	<xsl:if test="$rapportnavn='ST02' or $rapportnavn='ST03'">
		<xsl:apply-templates select="rapport" mode="status-bunn"/>
	</xsl:if>

	</body.content>
	<body.end>
	<tagline>
	<a href="mailto:valgdesk@ntb.no">valgdesk@ntb.no</a>
	</tagline>
	</body.end>
	</body>
	</nitf>
</xsl:template>

<!-- Template for tabellen -->
<xsl:template match="rapport">
	<xsl:choose>
		<!-- ST02 ST03-->
		<xsl:when test="$rapportnavn='ST02' or $rapportnavn='ST03'">
			<xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] = 1 or data[@navn='Partikode'] = 'Andre']" mode="ST02"/>
		</xsl:when>

		<xsl:when test="$rapportnavn='ST04'">
			<xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] = 1 or data[@navn='Partikode'] = 'Andre']" mode="ST06"/>
		</xsl:when>
		
		<!-- ST06 &lt; 3 and data[@navn='Partikategori'] != 0] -->
		<xsl:when test="$rapportnavn='ST06'">
			<xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] = 1 or data[@navn='Partikode'] = 'Andre']" mode="ST06"/>
		</xsl:when>
		
		<!-- F02 F03 K03 -->
		<!-- K05 K04 -->
		<xsl:otherwise>
			<xsl:apply-templates select="tabell/liste[data[@navn='Partikategori'] &lt; 3 and data[@navn='Partikategori'] != 0]" mode="K05"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- Template for kolonner i tabellen K02-->
<xsl:template match="liste" mode="ST02">
	<!-- K02 F04 -->
	<tr>
	<td>
	<xsl:value-of select="data[@navn='Partikode']"/>
	</td>
	<td align="right">
	<xsl:if test="data[@navn='AntStemmer']!=''">
		<xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
	</xsl:if>
	</td>
	<td align="right">
	<xsl:value-of select="data[@navn='ProSt']"/>
	</td>
	<td align="right">
	<xsl:value-of select="data[@navn='DiffPropFStv']"/>
	</td>
	<td align="right">
	<xsl:value-of select="data[@navn='DiffPropFFtv']"/>
	</td>
	
	<!--td align="right">
	<xsl:value-of select="data[@navn='ProgAntMndt']"/>
	<xsl:if test="data[@navn='DiffProgAntMndt']!=''">
		<xsl:text> (</xsl:text>
		<xsl:value-of select="data[@navn='DiffProgAntMndt']"/>
		<xsl:text>)</xsl:text>
	</xsl:if-->
	
	</tr>
</xsl:template>

<!-- Template for kolonner i tabellen ST06-->
<xsl:template match="liste" mode="ST06">
	<tr>
	<td>
	<xsl:value-of select="data[@navn='Partikode']"/>
	</td>
	<td align="right">
	<xsl:if test="data[@navn='AntStemmer']!=''">
		<xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
	</xsl:if>
	</td>
	<td align="right">
	<xsl:choose>
		<xsl:when test="$prognose = 'true'">
			<xsl:value-of select="data[@navn='ProgProSt']"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="data[@navn='ProSt']"/>
		</xsl:otherwise>
	</xsl:choose>
	</td>
	<td align="right">
	<xsl:choose>
		<xsl:when test="$prognose = 'true'">
			<xsl:value-of select="data[@navn='DiffProgPropFStv']"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="data[@navn='DiffPropFStv']"/>
		</xsl:otherwise>
	</xsl:choose>
	</td>
	<td align="right">
	<xsl:choose>
		<xsl:when test="$prognose = 'true'">
			<xsl:value-of select="data[@navn='DiffProgPropFFtv']"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="data[@navn='DiffPropFFtv']"/>
		</xsl:otherwise>
	</xsl:choose>
	</td>
	<td align="right">
	<xsl:choose>
		<xsl:when test="$prognose = 'true' or data[@navn='ProgAntMndtStv'] != ''">
			<xsl:value-of select="data[@navn='ProgAntMndtStv']"/>
			<xsl:text> (</xsl:text>
				<xsl:choose>
					<xsl:when test="data[@navn='DiffProgAntMndtFStv'] = '+0'">
						<xsl:text>-</xsl:text>
					</xsl:when>
					<xsl:otherwise>
						<xsl:value-of select="data[@navn='DiffProgAntMndtFStv']"/>
					</xsl:otherwise>
				</xsl:choose>
			<xsl:text>)</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text></xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	</td>
	</tr>
</xsl:template>

<!-- Template for kolonner i tabellen se-->
<xsl:template match="liste" mode="se">
	<!-- F02 F03 K03 -->
	<tr>
	<td>
	<xsl:value-of select="data[@navn='Partikode']"/>
	</td>
	<td align="right">
	<xsl:if test="data[@navn='AntStemmer']!=''">
		<xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
	</xsl:if>
	</td>
	<td align="right">
	<xsl:value-of select="data[@navn='ProSt']"/>
	</td>

	<xsl:choose>
		<xsl:when test="$rapportnavn='K03'">
			<td align="right">
			<xsl:value-of select="data[@navn='DiffPropFKsv']"/>
			</td>
			<td align="right">
			<xsl:value-of select="data[@navn='DiffPropFStv']"/>
			</td>
		</xsl:when>
		<xsl:otherwise>
			<td align="right">
			<xsl:value-of select="data[@navn='DiffPropFFtv']"/>
			</td>
			<td align="right">
			<xsl:value-of select="data[@navn='DiffPropFStv']"/>
			</td>
		</xsl:otherwise>
	</xsl:choose>
	</tr>
</xsl:template>

<!-- Template for kolonner i tabellen K05-->
<xsl:template match="liste" mode="K05">
	<!-- K05 K04-->
	<tr>
	<td>
	<xsl:value-of select="data[@navn='Partikode']"/>
	</td>
	<td align="right">
	<xsl:if test="data[@navn='AntStemmer']!=''">
		<xsl:value-of select="format-number(data[@navn='AntStemmer'], '### ##0', 'no')"/>
	</xsl:if>
	</td>
	<td align="right">
	<xsl:value-of select="data[@navn='ProSt']"/>
	</td>
	<td align="right">
	<xsl:value-of select="data[@navn='DiffPropFKsv']"/>
	</td>
	<td align="right">
	<xsl:value-of select="data[@navn='DiffPropFStv']"/>
	</td>
	</tr>
</xsl:template>

<!-- Template for tabellhode i tabellen -->
<xsl:template name="table_header">
	<tr class="header">
	<td>
	<xsl:text>Parti</xsl:text>
	</td>
	<td align="right">
	<xsl:text>Stemmer</xsl:text>
	</td>
	<td align="right">
	<xsl:text>Andel %</xsl:text>
	</td>
	<xsl:choose>
		<xsl:when test="$rapportnavn='ST06'">
			<td align="right">
			<xsl:text>05-01</xsl:text>
			</td>
			<td align="right">
			<xsl:text>05-03</xsl:text>
			</td>
			<td align="right">
			<xsl:text>Mand</xsl:text>
			</td>
			<!--
			<xsl:value-of select="$tab"/>
			<xsl:text>Mand. tenkt St.valg</xsl:text>
			-->
		</xsl:when>
		<xsl:when test="$rapportnavn='ST04'">
			<td align="right">
			<xsl:text>05-01</xsl:text>
			</td>
			<td align="right">
			<xsl:text>05-03</xsl:text>
			</td>
			<td align="right">
			<xsl:text>Mand</xsl:text>
			</td>
			<!--xsl:if test="$tabelltype='ov'">
				<td align="right">
				<xsl:text>Mand.</xsl:text>
				</td>
			</xsl:if-->
		</xsl:when>
		<xsl:when test="$rapportnavn='ST03'">
			<td align="right">
			<xsl:text>05-01</xsl:text>
			</td>
			<td align="right">
			<xsl:text>05-03</xsl:text>
			</td>
		</xsl:when>
		<xsl:when test="$rapportnavn='ST02'">
			<td align="right">
			<xsl:text>05-01</xsl:text>
			</td>
			<td align="right">
			<xsl:text>05-03</xsl:text>
			</td>
		</xsl:when>
		<xsl:otherwise>
		</xsl:otherwise>
	</xsl:choose>
	</tr>
</xsl:template>

<!-- Template for sum i tabellen -->
<xsl:template name="sum">
	<tr class="sum">
	<td>
	<xsl:text>Sum</xsl:text>
	</td>

	<td align="right">
	<!--<xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikode'] != 'Andre']/data[@navn='AntStemmer']), '### ##0', 'no')"/>-->
	<xsl:value-of select="format-number(/respons/rapport/data[@navn='AntFrammotte'], '### ##0', 'no')"/>
	</td>

	<td></td>
	<td></td>
	<td></td>
	<xsl:if test="$rapportnavn='ST04' or $rapportnavn='ST06'">
		<td></td>
	</xsl:if>
	</tr>
</xsl:template>

<!-- Template for sum i tabellen -->
<xsl:template name="sumandre">
	<tr>
	<td>
	<xsl:text>Andre</xsl:text>
	</td>
	<td align="right">
	<xsl:value-of select="format-number(sum(//liste[data/@navn='Partikode' and data[@navn='Partikategori'] = '3']/data[@navn='AntStemmer']), '### ##0', 'no')"/>
	</td>
	<td></td>
	<td></td>
	<xsl:if test="$tabelltype='ov'">
		<td></td>
	</xsl:if>
	<xsl:if test="$rapportnavn='K02' or $rapportnavn='K03' or $rapportnavn='F04' or $rapportnavn='F02'">
		<td></td>
	</xsl:if>

	</tr>
</xsl:template>

<!--
<xsl:variable name="crlf">
	<xsl:text>&#13;&#10;</xsl:text>
</xsl:variable>

<xsl:variable name="tab">
	<xsl:text>&#9;</xsl:text>
</xsl:variable>
-->

<xsl:template match="rapport" mode="status">
	<!--
		435 av 435 kommuner (435 ferdig opptalt).€
		100,0 prosent av ant. stemmeberettigede.€
		2521781 opptalte stemmer.€
		Frammøte: 39,3 prosent.€
	-->
	<xsl:if test="data[@navn='TotAntKomm']">
		<p>
		<xsl:value-of select="data[@navn='AntKommFhstOpptalt'] + data[@navn='AntKommVtstOpptalt'] + data[@navn='AntKommAltOpptalt']"/>
		<xsl:text> av </xsl:text>
		<xsl:value-of select="data[@navn='TotAntKomm']"/>
		<xsl:text> kommuner (</xsl:text>
		<xsl:value-of select="data[@navn='AntKommAltOpptalt']"/>
		<xsl:text> ferdig opptalt).</xsl:text>
		</p>
	</xsl:if>

	<p>
	<xsl:text>Omfatter </xsl:text>
	<xsl:if test="data[@navn='AntStBerett']">
		<xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStBerett'] * 100), '#0,0', 'pros')"/>
	</xsl:if>
	<xsl:if test="data[@navn='AntStberett']">
		<xsl:value-of select="format-number((data[@navn='AntFrammotte'] div data[@navn='AntStberett'] * 100), '#0,0', 'pros')"/>
	</xsl:if>
	<xsl:text> prosent av </xsl:text>
	<xsl:choose>
		<xsl:when test="data[@navn='AntStBerett']">
			<xsl:value-of select="format-number(data[@navn='AntStBerett'], '### ### ###', 'no')"/>
		</xsl:when>
		<xsl:otherwise>
			<xsl:value-of select="format-number(data[@navn='AntStberett'], '### ### ###', 'no')"/>
		</xsl:otherwise>
	</xsl:choose>
	<xsl:text> stemmeberettigede.</xsl:text>
	</p>
	<p>
	<xsl:value-of select="format-number(data[@navn='AntFrammotte'], '### ### ###', 'no')"/>
	<xsl:text> opptalte stemmer.</xsl:text>
	</p>
	<p>
	<xsl:text>Frammøte: </xsl:text>
	<xsl:value-of select="data[@navn='ProFrammotte']"/>
	<xsl:text> prosent.</xsl:text>
	</p>

</xsl:template>

<xsl:template match="rapport" mode="status-bunn">

	<p>
	<xsl:text>Antall st.ber.: </xsl:text>
	<xsl:if test="data[@navn='AntStBerett']">
		<xsl:value-of select="format-number(data[@navn='AntStBerett'], '### ### ###', 'no')"/>
	</xsl:if>
	<xsl:text>.</xsl:text>
	</p>
	<p>
	<xsl:text>Frammøte: </xsl:text>
	<xsl:value-of select="data[@navn='ProFrammotte']"/>
	<xsl:text> prosent.</xsl:text>
	</p>
	<p>
	<xsl:choose>
		<xsl:when test="$statusind = '5'">
			<xsl:text>Foreløpig resultat.</xsl:text>
		</xsl:when>
		<xsl:when test="$statusind = '6'">
			<xsl:text>Alle forhåndsstemmer opptalt.</xsl:text>
		</xsl:when>
		<xsl:when test="$statusind = '7'">
			<xsl:text>Alt opptalt untatt enkelte forhåndsstemmer.</xsl:text>
		</xsl:when>
		<xsl:when test="$statusind = '8'">
			<xsl:text>Alt opptalt.</xsl:text>
		</xsl:when>
		<xsl:otherwise></xsl:otherwise>		
	</xsl:choose>
	</p>

</xsl:template>

<xsl:variable name="kanal">
	<xsl:choose>
	<!--<xsl:when test="/respons/rapport/rapportnavn[.='F05' or .='K05' or .='F04' or .='K04']">A</xsl:when>-->
	<xsl:when test="substring-before(/respons/rapport/rapportnavn, '0')='F'">A</xsl:when>
	<xsl:otherwise>C</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:template name="NitfHead">
	<head>
	<title><xsl:value-of select="$infolinje"/></title>

	<meta name="timestamp">
		<xsl:attribute name="content"><xsl:value-of select="$normdate"/></xsl:attribute>
	</meta>
	<meta name="subject" >
		<xsl:attribute name="content"><xsl:value-of select="$stikkord"/></xsl:attribute>
	</meta>
	<meta name="foldername" content="Ut-Satellitt" />
	<meta name="filename" >
		<xsl:attribute name="content"><xsl:value-of select="$filnavn"/></xsl:attribute>
	</meta>
	<meta name="NTBTjeneste" content="Nyhetstjenesten" />
	<meta name="NTBMeldingsSign" content="valget" />
	<meta name="NTBMeldingsType" content="Valg" />
	<meta name="NTBPrioritet" >
		<xsl:attribute name="content"><xsl:value-of select="$urgency"/></xsl:attribute>
	</meta>
	<meta name="NTBStikkord">
		<xsl:attribute name="content"><xsl:value-of select="$stikkord"/></xsl:attribute>
	</meta>
	<meta name="NTBBilderAntall" content="0" />
	<meta name="NTBDistribusjonsKode" content="ALL" />
	<meta name="NTBKanal" content="A" />
	<meta name="NTBSendTilDirekte" content="False" />
	<meta name="NTBIPTCSequence" content="0000" />
	<meta name="NTBID">
		<xsl:attribute name="content"><xsl:value-of select="$nitfdate"/>_<xsl:value-of select="$rapportnavn"/>
		<xsl:choose>
			<xsl:when test="$rapportnavn='ST02'">
				<xsl:text>_</xsl:text>
				<xsl:value-of select="$kommunenr"/>
			</xsl:when>
			<xsl:when test="$rapportnavn='ST03'">
				<xsl:text>_</xsl:text>
				<xsl:value-of select="$kommunenr"/>
				<xsl:text>_</xsl:text>
				<xsl:value-of select="$kretsnr"/>
			</xsl:when>
			<xsl:when test="$rapportnavn='ST04'">
				<xsl:text>_</xsl:text>
				<xsl:value-of select="$fylkenr"/>
			</xsl:when>
		</xsl:choose>
		</xsl:attribute>
	</meta>
	<meta name="ntb-dato">
		<xsl:attribute name="content"><xsl:value-of select="$ntbdato"/></xsl:attribute>
	</meta>

	<meta name="rapportnavn">
		<xsl:attribute name="content"><xsl:value-of select="/respons/rapport/rapportnavn"/></xsl:attribute>
	</meta>
	<meta name="status-ind">
		<xsl:attribute name="content"><xsl:value-of select="/respons/rapport/data[@navn='StatusInd']"/></xsl:attribute>
	</meta>
	<meta name="fylkenr">
		<xsl:attribute name="content"><xsl:value-of select="/respons/rapport/data[@navn='FylkeNr']"/></xsl:attribute>
	</meta>
	<meta name="fylkenavn">
		<xsl:attribute name="content"><xsl:value-of select="$fylke"/></xsl:attribute>
	</meta>
	<meta name="kommunenr">
		<xsl:attribute name="content"><xsl:value-of select="/respons/rapport/data[@navn='KommNr']"/></xsl:attribute>
	</meta>
	<meta name="kommunenavn">
		<xsl:attribute name="content"><xsl:value-of select="$kommune"/></xsl:attribute>
	</meta>
	<meta name="kretsnr">
		<xsl:attribute name="content"><xsl:value-of select="/respons/rapport/data[@navn='KretsNr']"/></xsl:attribute>
	</meta>
	<meta name="kretsnavn">
		<xsl:attribute name="content"><xsl:value-of select="$krets"/></xsl:attribute>
	</meta>
	<meta name="tabelltype">
		<xsl:attribute name="content"><xsl:value-of select="$tabelltype"/></xsl:attribute>
	</meta>

	<tobject tobject.type="Innenriks">
	<tobject.property tobject.property.type="Tabeller og resultater" />
	<tobject.subject tobject.subject.refnum="11000000" tobject.subject.code="POL" tobject.subject.type="Politikk" />
	</tobject>
	<docdata>
	<evloc state-prov="Norge" />
	<evloc state-prov="Norge" county-dist="Riksnyheter" />
	<doc-id regsrc="NTB">
		<xsl:attribute name="id-string"><xsl:value-of select="$nitfdate"/>_<xsl:value-of select="$rapportnavn"/>
		<xsl:choose>
			<xsl:when test="$rapportnavn='ST02'">
				<xsl:text>_</xsl:text>
				<xsl:value-of select="$kommunenr"/>
			</xsl:when>
			<xsl:when test="$rapportnavn='ST03'">
				<xsl:text>_</xsl:text>
				<xsl:value-of select="$kommunenr"/>
				<xsl:text>_</xsl:text>
				<xsl:value-of select="$kretsnr"/>
			</xsl:when>
			<xsl:when test="$rapportnavn='ST04'">
				<xsl:text>_</xsl:text>
				<xsl:value-of select="$fylkenr"/>
			</xsl:when>
		</xsl:choose>
		</xsl:attribute>
	</doc-id>
	<urgency>
	<xsl:attribute name="ed-urg"><xsl:value-of select="$urgency"/></xsl:attribute>
	</urgency>
	<date.issue>
		<xsl:attribute name="norm"><xsl:value-of select="translate($normdate,'. ','-T')"/></xsl:attribute>
	</date.issue>
	<ed-msg info="" />
	<du-key version="1">
		<xsl:attribute name="key"><xsl:value-of select="$stikkord"/></xsl:attribute>
	</du-key>
	<doc.copyright year="2005" holder="NTB" />
	<key-list>
	<keyword>
		<xsl:attribute name="key"><xsl:value-of select="$stikkord"/></xsl:attribute>
	</keyword>
	</key-list>
	</docdata>
	<pubdata date.publication="" item-length="0" unit-of-measure="character">
	<xsl:attribute name="date.publication">
		<xsl:value-of select="$nitfdate"/>
	</xsl:attribute>
	<!--
	<xsl:attribute name="item-length">
		<xsl:value-of select="string-length(/respons/rapport/tabell)"/>
	</xsl:attribute>
	-->
	</pubdata>
	<revision-history name="valget" />
	</head>
</xsl:template>


<xsl:variable name="rapportnavn">
	<xsl:value-of select="/respons/rapport/rapportnavn"/>
</xsl:variable>

<xsl:variable name="rapport">
	<xsl:value-of select="substring-after(/respons/rapport/rapportnavn, '0')"></xsl:value-of>
</xsl:variable>

<xsl:variable name="rapporttype">
	<xsl:choose>
		<xsl:when test="$rapport='2'">KOM</xsl:when>
		<xsl:when test="$rapport='3'">KRE</xsl:when>
		<xsl:otherwise>OVS</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="date">
	<xsl:value-of select="/respons/rapport/data[@navn='SisteRegDato']"/>
</xsl:variable>

<xsl:variable name="time">
	<xsl:value-of select="/respons/rapport/data[@navn='SisteRegTid']"/>
</xsl:variable>

<xsl:variable name="dato">
	<xsl:value-of select="substring($date, 7, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 5, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 1, 4)"/>
</xsl:variable>

<xsl:variable name="tid">
	<xsl:value-of select="substring($time, 1, 5)"/>
</xsl:variable>

<xsl:variable name="nitfdate">
	<xsl:value-of select="$date"/>
	<xsl:text>T</xsl:text>
	<xsl:value-of select="substring($time, 1, 2)"/>
	<xsl:value-of select="substring($time, 4, 2)"/>
	<xsl:value-of select="substring($time, 7, 2)"/>
</xsl:variable>

<xsl:variable name="valgtype">
	<xsl:text>Stortingsvalg</xsl:text>
	<!--
	<xsl:choose>
		<xsl:when test="substring-before($rapportnavn, '0') = 'F'">
			<xsl:text>Fylkestingsvalg</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>Kommunestyrevalg</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
	-->
</xsl:variable>

<xsl:param name="ntbdato">
	<xsl:value-of select="$dato"/>
	<xsl:text> </xsl:text>
	<xsl:value-of select="substring($time, 1, 5)"/>
</xsl:param>

<xsl:param name="normdate">
	<xsl:value-of select="substring($date, 1, 4)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 5, 2)"/>
	<xsl:text>.</xsl:text>
	<xsl:value-of select="substring($date, 7, 2)"/>
	<xsl:text> </xsl:text>
	<xsl:value-of select="$time"/>
</xsl:param>

<xsl:variable name="stikkord">
	<!-- stikkord -->
	<xsl:text>VLG-</xsl:text>
	<xsl:value-of select="$rapportnavn"/>-<xsl:value-of select="$rapport-type"/>
</xsl:variable>

<xsl:variable name="filnavn">
	<!-- stikkord -->
	<xsl:value-of select="$rapportnavn"/>
	
	<xsl:if test="$fylkenr != ''">
		<xsl:text>-</xsl:text>
		<xsl:value-of select="$fylkenr"/>
	</xsl:if>
	
	<xsl:if test="$kommunenr != ''">
	<xsl:text>-</xsl:text>
	<xsl:value-of select="$kommunenr"/>
	</xsl:if>

	<xsl:if test="$kretsnr != ''">
	<xsl:text>-</xsl:text>
	<xsl:value-of select="$kretsnr"/>
	</xsl:if>
	<xsl:text>.xml</xsl:text>
</xsl:variable>

<xsl:variable name="urgency">
	<xsl:choose>
		<xsl:when test="$rapport ='6' or $rapport ='4'">4</xsl:when>
		<xsl:otherwise>6</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="statusind">
	<xsl:value-of select="/respons/rapport/data[@navn='StatusInd']"/>
</xsl:variable>

<xsl:variable name="stedsnavn">
	<!-- Hent Fylkesnavn, Kommunenavn og Kretsnavn -->
	<xsl:choose>
		<xsl:when test="$rapportnavn='ST02'">
			<xsl:value-of select="$kommune"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='ST04'">
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='ST03'">
			<xsl:value-of select="$krets"/>
			<xsl:text> i </xsl:text>
			<xsl:value-of select="$kommune"/>
		</xsl:when>
		<xsl:otherwise>Landsoversikt</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="rapport-type">
	<!-- Hent Fylkesnavn, Kommunenavn og Kretsnavn -->
	<xsl:choose>
		<xsl:when test="$rapportnavn='ST02'">
			<!--<xsl:text>komm-</xsl:text>-->
			<xsl:value-of select="$kommunenr"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="$kommune"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='ST04'">
			<!--<xsl:text>fylke-</xsl:text>-->
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='ST03'">
			<!--<xsl:text>krets-</xsl:text>-->
			<xsl:value-of select="$kommune"/>
			<xsl:text>-</xsl:text>
			<xsl:value-of select="$krets"/>
		</xsl:when>
		<xsl:when test="$rapportnavn='ST06'">
			<xsl:text>landsoversikt</xsl:text>
		</xsl:when>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="tabelltype">
	<xsl:choose>
		<xsl:when test="$rapportnavn = 'K02'">
			<xsl:text>ov</xsl:text>
		</xsl:when>
		<xsl:when test="substring($rapportnavn, 2, 2) = '02' or substring($rapportnavn, 2, 2) = '03'">
			<xsl:text>se</xsl:text>
		</xsl:when>
		<xsl:otherwise>
			<xsl:text>ov</xsl:text>
		</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="infolinje">
	<xsl:value-of select="$valgtype"/>
	<xsl:text>: </xsl:text>
	<xsl:choose>
		<!--
		<xsl:when test="$rapportnavn = 'F01' or $rapportnavn = 'K01'">
			<xsl:text>Opptalte kommuner</xsl:text>
		</xsl:when>
		-->
		<xsl:when test="$rapportnavn = 'ST02'">
			<xsl:text></xsl:text>
			<xsl:value-of select="$fylke"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="$kommune"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'ST03'">
			<xsl:value-of select="$kommune"/>
			<xsl:text>, </xsl:text>
			<xsl:value-of select="$krets"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'ST04'">
			<xsl:text>Fylkesoversikt, </xsl:text>
			<xsl:value-of select="$fylke"/>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'ST06'">
			<xsl:text>Landsoversikt</xsl:text>
		</xsl:when>
		<!--
		<xsl:when test="$rapportnavn = 'F07' or $rapportnavn = 'K07'">
			<xsl:text>Landsoversikt pr. fylke</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K08'">
			<xsl:text>Bydelsresultater i Oslo</xsl:text>
		</xsl:when>
		<xsl:when test="$rapportnavn = 'K09'">
			<xsl:text>Bystyreoversikt</xsl:text>
		</xsl:when>
		-->
	</xsl:choose>

	<xsl:if test="$prognose = 'true'">
		<xsl:text>, prognose</xsl:text>
	</xsl:if>

	<xsl:text>, kl. </xsl:text>
	<xsl:value-of select="$tid"/>
	<xsl:if test="$statusind &gt; 5">
		<xsl:text>. Korrigert resultat</xsl:text>
	</xsl:if>
</xsl:variable>

</xsl:stylesheet>