﻿Imports System.Web.Security
Imports System.Runtime.InteropServices


Public Class login
    Inherits System.Web.UI.Page

    <DllImport("ADVAPI32.dll")> _
    Public Shared Function LogonUser(ByVal lpszUsername As String, ByVal lpszDomain As String, ByVal lpszPassword As String,
                                              ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, ByRef phToken As IntPtr) As Boolean
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

#If DEBUG Then
        If Not String.IsNullOrEmpty(Request("data")) Then

            Response.Write("OK")
            Response.End()

        End If
#End If

        'TODO: Add some ip check here, before auto logon for internal users

        'Autorenew is not needed. TTL is extracted from web.config, same exipry for Cookie and Auth ticket

    End Sub

    Protected Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click

        Dim domainName As String = ConfigurationManager.AppSettings("logonDomain")
        Dim userName As String = txtUserName.Text
        Dim token As IntPtr = IntPtr.Zero

        '<DllImport("ADVAPI32.dll")> _
        'Public Shared Function LogonUser(ByVal lpszUsername As String, ByVal lpszDomain As String, ByVal lpszPassword As String,
        '                                          ByVal dwLogonType As Integer, ByVal dwLogonProvider As Integer, ByRef phToken As IntPtr) As Boolean
        'End Function

        'userName, domainName and Password parameters are very obvious.
        '
        'dwLogonType (3rd parameter): I used LOGON32_LOGON_INTERACTIVE, This logon type is
        'intended for users who will be interactively using the computer, such as a user being
        'logged on by a terminal server, remote shell, or similar process. This logon type has
        'the additional expense of caching logon information for disconnected operations. For
        '.more details about this parameter please see http://msdn.microsoft.com/en-us/library/aa378184(VS.85).aspx */
        '/* dwLogonProvider (4th parameter) : I used LOGON32_PROVIDER_DEFAUL, This provider
        'uses the standard logon provider for the system. The default security provider is
        'negotiate, unless you pass NULL for the domain name and the user name is not in UPN
        'format. In this case, the default provider is NTLM. For more details about this
        'parameter please see http://msdn.microsoft.com/en-us/library/aa378184(VS.85).aspx */
        '/* phToken (5th parameter): A pointer to a handle variable that receives a handle to
        'a token that represents the specified user. We can use this handler for impersonation
        'purpose. */
        Dim res As Boolean = LogonUser(userName, domainName, txtPassword.Text, 2, 0, token)

        If (res) Then
            'If Sucessfully authenticated
            FormsAuthentication.RedirectFromLoginPage(txtUserName.Text, RememberMe.Checked)
        Else
            'If not authenticated then display an error message
            lblStatus.ForeColor = Color.Red
            lblStatus.Text = "Brukernavn eller passord er ikke gyldig."
        End If
    End Sub
End Class